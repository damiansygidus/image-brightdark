# image-brightdark App for scalac

**Are you afraid of the dark?** Version: 1.1

Description of this task is described in *Are you afraid of the dark.pdf* file.\
Source code of the solution is located at `src/main/scala/com/scalac/`\
Tests are located at `src/test/scala/com/scalac/`

### App startup

The app requires two folders for input and output. Input folder name/path can be assigned using first argument at startup and output folder can be assigned using second argument. Last (third) argument at startup is used to setup the "cut off" (breakpoint) score value.

Application requires three startup arguments:
1. Input folder name/path
2. Export folder name/path
3. Breakpoint ("cut off") score value

**All arguments are mandatory**


Example for this project on mac/linux:
```bash 
sbt compile
sbt "run in out 20"
``` 

### App test

To validate proper behaviour two tests were added to this project. First tests an image with all white pixels and second test an image with all black pixels.

To run tests for this project on mac/linux:
```bash 
sbt test
```