package com.scalac

import java.awt.image.BufferedImage

import com.sun.scenario.effect.ImageData
import org.scalatest.FlatSpec

class LumCalculatorSpec extends FlatSpec {
  "LumCalculator" should "return value 100 for completely white image" in {
    val whiteImage = createMockImage(256, 256, 255)

    assert(LumCalculator.calculate(whiteImage) == 100)
  }

  "LumCalculator" should "return value 0 for completely black image" in {
    val backImage = createMockImage(256, 256, 0)

    assert(LumCalculator.calculate(backImage) == 0)
  }

  def createMockImage(width: Int, height: Int, pixelColor: Int): ImageData = {
    val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

    var y = 0
    for (y <- 0 until height) {
      var x = 0
      for (x <- 0 until width) {
        val p = (pixelColor << 16) | (pixelColor << 8) | pixelColor
        img.setRGB(x, y, p)
      }
    }

    new ImageData("blackImage", "jpg", img)
  }
}
