package com.scalac

object Controller {

  def main(args: Array[String]): Unit = {
    val IMPORT_PATH = args(0)
    val EXPORT_PATH = args(1)
    val BRIGHTNESS_BREAKPOINT = args(2).toInt
    val IMAGE_EXTENSIONS = Array[String]("jpg", "jpeg", "png")
    val LABEL = Map(
      "dark" -> "dark",
      "bright" -> "bright"
    )

    try {
      val importData = new Import(IMPORT_PATH, IMAGE_EXTENSIONS)
      val exportData = new Export(EXPORT_PATH)
      exportData.deleteOldFolderContents()

      val imagesData = importData.importArrayOfImageData()
      println("Images imported: " + imagesData.size)

      val timer = new Timer

      timer.startTimer()

      for (i <- imagesData.par) {
        val brightness = LumCalculator.calculate(i)
        var extraText = ""
        if (brightness > BRIGHTNESS_BREAKPOINT) {
          extraText = "_" + LABEL("bright") + "_" + brightness
        } else {
          extraText = "_" + LABEL("dark") + "_" + brightness
        }
        exportData.saveImage(i, extraText)
      }

      println("Calculation time: " + timer.stepTimer())
    } catch {
      case e: Exception =>
        System.out.println("ERROR: " + e)
    }
  }
}
