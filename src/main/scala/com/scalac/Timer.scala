package com.scalac

import java.text.SimpleDateFormat
import java.util.Calendar

class Timer {
  val format = new SimpleDateFormat("mm:ss:SSS")
  var time: Long = 0

  def startTimer(): Unit = {
    time = Calendar.getInstance().getTimeInMillis
  }

  def stepTimer(): String = {
    val duration = Calendar.getInstance().getTimeInMillis - time
    format.format(duration)
  }
}
