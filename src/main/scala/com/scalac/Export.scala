package com.scalac

import java.io.File

import javax.imageio.ImageIO

import scala.reflect.io.Directory

class Export(dirPath: String) {
  val dir: String = dirPath

  def deleteOldFolderContents(): Unit = {
    val directory = new Directory(new File(dir))
    directory.deleteRecursively()
    directory.createDirectory()
  }

  def saveImage(imageData: ImageData, extraText: String): Unit = {
    val output = new File(dir + "/" + imageData.name + extraText + "." + imageData.format)
    ImageIO.write(imageData.image, imageData.format, output)
  }
}
