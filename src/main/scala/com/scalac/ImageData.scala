package com.scalac

import java.awt.image.BufferedImage

class ImageData(fileName: String, fileFormat: String, imageFile: BufferedImage) {
  val name: String = fileName
  val format: String = fileFormat
  val image: BufferedImage = imageFile
}
