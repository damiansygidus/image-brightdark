package com.scalac

import java.awt.Color

object LumCalculator {
  def calculate(i: ImageData): Int = {
    val width = i.image.getWidth
    val height = i.image.getHeight
    var pixelBrightnessSum = 0.0

    var y = 0
    for (y <- 0 until height) {
      var x = 0
      for (x <- 0 until width) {
        val c = new Color(i.image.getRGB(x, y))
        pixelBrightnessSum += BigDecimal(0.2126 * c.getRed.toDouble + 0.7152 * c.getGreen.toDouble + 0.0722 * c.getBlue.toDouble)
          .setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
        // Relative luminance formula: https://en.wikipedia.org/wiki/Relative_luminance
      }
    }

    BigDecimal((pixelBrightnessSum / (width * height)) / 255 * 100)
      .setScale(0, BigDecimal.RoundingMode.HALF_UP).toInt
  }
}
