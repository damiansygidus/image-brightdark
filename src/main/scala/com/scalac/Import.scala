package com.scalac

import java.io.{File, FilenameFilter, IOException}

import javax.imageio.ImageIO

import scala.collection.mutable.ArrayBuffer

class Import(dirPath: String, fileExtensions: Array[String]) {
  val dir = new File(dirPath)
  val extensions: Array[String] = fileExtensions
  val imageFilter: FilenameFilter = new FilenameFilter() {
    override def accept(dir: File, name: String): Boolean = {
      for (ext <- extensions) {
        if (name.endsWith("." + ext)) return true
      }
      false
    }
  }

  def importArrayOfImageData(): ArrayBuffer[ImageData] = {
    val imagesData = ArrayBuffer[ImageData]()
    if (dir.isDirectory) {
      for (f <- dir.listFiles(imageFilter)) {
        try {
          val indexOfDot = f.getName.lastIndexOf(".")
          val fileName = f.getName.substring(0, indexOfDot)
          val formatName = f.getName.substring(indexOfDot + 1)

          imagesData += new ImageData(fileName, formatName, ImageIO.read(f))
        } catch {
          case e: IOException =>
            System.out.println("ERROR: " + e)
        }
      }
    }
    imagesData
  }
}
